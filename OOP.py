from abc import ABC, abstractmethod
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from graphviz import Graph
from KPIs import *


class Forecast(ABC):

    def __init__(self, name, y_len):
        self.name = name
        self.y_len = y_len
        self.pred_ho = []
        self.pred_test = []
        self.error_ho = []
        self.mae_ho = None
        self.rmse_ho = None
        self.bias_ho = None
        self.mae_test = None
        self.rmse_test = None
        self.bias_test = None
        self.weight = 1

    @abstractmethod
    def predict(self):
        pass

    def compute_error(self, y_ho):
        self.error_ho = y_ho - self.pred_ho

    def computeKPIs(self, y_ho, y_test):
        self.mae_ho = MAE(y_ho, self.pred_ho)
        self.rmse_ho = RMSE(y_ho, self.pred_ho)
        self.bias_ho = bias(y_ho, self.pred_ho)
        self.mae_test = MAE(y_test, self.pred_test)
        self.rmse_test = RMSE(y_test, self.pred_test)
        self.bias_test = bias(y_test, self.pred_test)
        self.compute_error(y_ho)

    def update_weight(self, weight):
        self.weight *= weight


class Model(Forecast, ABC):

    def __init__(self, name, x_len, y_len):
        Forecast.__init__(self, name, y_len)
        self.x_len = x_len
        self.model = None
        self.pred_train = []

    @abstractmethod
    def train(self, x_train, y_train):
        pass


class ClassicModel(Model, ABC):

    def __init__(self, name, x_len, y_len):
        Model.__init__(self, name, x_len, y_len)

    def predict(self, x_train, x_ho, x_test):
        if x_train != []:
            self.pred_train = self.model.predict(x_train)
        self.pred_ho = self.model.predict(x_ho)
        self.pred_test = self.model.predict(x_test)


class Tree(ClassicModel):

    def __init__(self, x_len, y_len):
        name = "Tree" + str(x_len)
        Model.__init__(self, name, x_len, y_len)

    def train(self, x_train, y_train):
        from sklearn.tree import DecisionTreeRegressor

        print("Training " + self.name)

        if self.x_len == 24:
            tree_features = {"min_samples_leaf": 9, "max_depth": 6}
        elif self.x_len == 12:
            tree_features = {"min_samples_leaf": 6, "max_depth": 6}
        else:
            raise AttributeError("x_len != 12 or 24")

        tree = DecisionTreeRegressor(**tree_features)
        tree.fit(x_train, y_train)
        self.model = tree

    def predict(self):
        self.pred_train = pd.read_excel("DeterministicForecasts/preds_" + self.name + "_train.xlsx", index_col=0).values
        self.pred_ho = pd.read_excel("DeterministicForecasts/preds_" + self.name + "_ho.xlsx", index_col=0).values
        self.pred_test = pd.read_excel("DeterministicForecasts/preds_" + self.name + "_test.xlsx", index_col=0).values


class Forest(ClassicModel):

    def __init__(self, x_len, y_len):
        name = "Forest" + str(x_len)
        Model.__init__(self, name, x_len, y_len)

    def train(self, x_train, y_train):
        from sklearn.ensemble import RandomForestRegressor

        print("Training " + self.name)

        if self.x_len == 12:
            forest_features = {"max_features": 5,
                               "max_depth": 7,
                               "min_samples_split": 3,
                               "min_samples_leaf": 8,
                               "bootstrap": True}
        elif self.x_len == 24:
            forest_features = {"max_features": min(7, self.x_len),
                               "max_depth": 11,
                               "min_samples_split": 7,
                               "min_samples_leaf": 13,
                               "bootstrap": False}
        else:
            raise AttributeError("x_len != 12 or 24")

        forest = RandomForestRegressor(n_jobs=-1, n_estimators=200,
                                       **forest_features)
        forest.fit(x_train, y_train)
        self.model = forest


class ETR(ClassicModel):

    def __init__(self, x_len, y_len):
        name = "ETR" + str(x_len)
        Model.__init__(self, name, x_len, y_len)

    def train(self, x_train, y_train):
        from sklearn.ensemble import ExtraTreesRegressor

        print("Training " + self.name)

        if self.x_len == 12:
            ETR_features = {"max_features": 8,
                            "max_depth": 14,
                            "min_samples_split": 4,
                            "min_samples_leaf": 4,
                            "bootstrap": False}
        elif self.x_len == 24:
            ETR_features = {"max_features": min(10, self.x_len),
                            "max_depth": 12,
                            "min_samples_split": 8,
                            "min_samples_leaf": 2,
                            "bootstrap": False}
        else:
            raise AttributeError("x_len != 12 or 24")

        etr = ExtraTreesRegressor(n_jobs=-1, **ETR_features,
                                  n_estimators=200)
        etr.fit(x_train, y_train)
        self.model = etr


class AdaBoost(ClassicModel):

    def __init__(self, x_len, y_len):
        name = "Ada" + str(x_len)
        Model.__init__(self, name, x_len, y_len)

    def train(self, x_train, y_train):
        from sklearn.ensemble import AdaBoostRegressor
        from sklearn.tree import DecisionTreeRegressor
        from sklearn.multioutput import MultiOutputRegressor

        print("Training " + self.name)

        if self.x_len == 12:
            max_depth = 8
            ada_features = {"n_estimators": 140,
                            "learning_rate": 0.001}
        elif self.x_len == 24:
            max_depth = 10
            ada_features = {"n_estimators": 130,
                            "learning_rate": 0.001}
        else:
            raise AttributeError("x_len != 12 or 24")

        tree = DecisionTreeRegressor(max_depth=max_depth)
        ada = AdaBoostRegressor(tree, **ada_features)
        if self.y_len != 1:
            ada = MultiOutputRegressor(ada, n_jobs=-1)
        ada.fit(x_train, y_train)
        self.model = ada


class GBoost(ClassicModel):

    def __init__(self, x_len, y_len):
        name = "GBoost" + str(x_len)
        Model.__init__(self, name, x_len, y_len)

    def train(self, x_train, y_train):
        from sklearn.ensemble import GradientBoostingRegressor
        from sklearn.multioutput import MultiOutputRegressor

        print("Training " + self.name)

        if self.x_len == 12:
            gbr_features = {'n_estimators': 60,
                            'max_features': 0.8,
                            'max_depth': 6,
                            'min_samples_split': 7,
                            'min_samples_leaf': 6}
        elif self.x_len == 24:
            gbr_features = {'n_estimators': 180,
                            'max_features': 0.9,
                            'max_depth': 5,
                            'min_samples_split': 4,
                            'min_samples_leaf': 1}
        else:
            raise AttributeError("x_len != 12 or 24")

        gbr = GradientBoostingRegressor(**gbr_features)
        if len(y_train.shape) != 1:
            gbr = MultiOutputRegressor(gbr, n_jobs=-1)
        gbr.fit(x_train, y_train)
        self.model = gbr


class XGBoost(ClassicModel):

    def __init__(self, x_len, y_len):
        name = "XGBoost" + str(x_len)
        Model.__init__(self, name, x_len, y_len)

    def train(self, x_train, y_train):
        from xgboost.sklearn import XGBRegressor
        from sklearn.multioutput import MultiOutputRegressor

        print("Training " + self.name)

        if self.x_len == 12:
            xgb_features = {"max_depth": 3,
                            "learning_rate": 0.01,
                            "colsample_bylevel": 0.5,
                            "subsample": 1.0,
                            "n_estimators": 1000}
        elif self.x_len == 24:
            xgb_features = {"max_depth": 4,
                            "learning_rate": 0.01,
                            "colsample_bylevel": 0.5,
                            "subsample": 0.8,
                            "n_estimators": 1000}
        else:
            raise AttributeError("x_len != 12 or 24")

        xgb = XGBRegressor(**xgb_features)
        if self.y_len != 1:
            xgb = MultiOutputRegressor(xgb, n_jobs=-1)
        xgb.fit(x_train, y_train)
        self.model = xgb

    def predict(self):
        self.pred_train = pd.read_excel("DeterministicForecasts/preds_"+self.name+"_train.xlsx", index_col=0).values
        self.pred_ho = pd.read_excel("DeterministicForecasts/preds_"+self.name+"_ho.xlsx", index_col=0).values
        self.pred_test = pd.read_excel("DeterministicForecasts/preds_"+self.name+"_test.xlsx", index_col=0).values


class NN(Model):

    def __init__(self, x_len, y_len):
        name = "NN" + str(x_len)
        Model.__init__(self, name, x_len, y_len)

    def train(self, x_train, y_train):
        from joblib import Parallel, delayed
        from sklearn.neural_network import MLPRegressor

        print("Training " + self.name)

        def train_model(model, x, y):
            return model.fit(x, y)

        if self.x_len == 12:
            NN_features = {'max_iter': 15, 'learning_rate_init': 0.02, 'learning_rate': 'constant',
                           'hidden_layer_sizes': (35, 35), 'early_stopping': True}
        elif self.x_len == 24:
            NN_features = {'max_iter': 15, 'learning_rate_init': 0.002, 'learning_rate': 'adaptive',
                           'hidden_layer_sizes': (45, 45, 45, 45), 'early_stopping': True}
        else:
            raise AttributeError("x_len != 12 or 24")

        result = Parallel(n_jobs=-1, backend="threading")(
            delayed(train_model)(MLPRegressor(**NN_features), x_train, y_train) for n in range(40))
        self.model = result

    def predict(self, x_train, x_ho, x_test):
        if x_train != []:
            pred = 0
            for n in self.model:
                pred += n.predict(x_train)
            self.pred_train = pred / 40

        pred = 0
        for n in self.model:
            pred += n.predict(x_ho)
        self.pred_ho = pred / 40

        pred = 0
        for n in self.model:
            pred += n.predict(x_test)
        self.pred_test = pred / 40


class ETS(Forecast):

    def __init__(self):
        Forecast.__init__(self, "ETS", 3)

    def predict(self):
        pred_ho = pd.read_excel("DeterministicForecasts/preds_ETS_ho.xlsx", index_col=0).values
        self.pred_ho = np.nan_to_num(pred_ho)
        pred_test = pd.read_excel("DeterministicForecasts/preds_ETS_test.xlsx", index_col=0).values
        self.pred_test = np.nan_to_num(pred_test)


class PlainAvg(Forecast):

    def __init__(self, models):
        self.countModels = len(models)
        name = "PlainAvg"+str(self.countModels)
        Forecast.__init__(self, name, models[0].y_len)
        self.predict(models)

    def predict(self, models):
        pred_ho = 0
        pred_test = 0

        for model in models:
            pred_ho += model.pred_ho
            pred_test += model.pred_test

        self.pred_ho = pred_ho / self.countModels
        self.pred_test = pred_test / self.countModels


class WgtAvg(Forecast):

    def __init__(self, models):
        self.countModels = len(models)
        name = "WgtAvg"+str(self.countModels)
        Forecast.__init__(self, name, models[0].y_len)
        self.predict(models)

    def predict(self, models):
        pred_ho = 0
        pred_test = 0

        maes = []
        for model in models:
            maes.append(model.mae_ho)

        norm = 0
        mean = np.mean(maes)

        for model in models:
            w = np.exp(mean - model.mae_ho)
            pred_ho += model.pred_ho * w
            pred_test += model.pred_test * w
            norm += w

        self.pred_ho = pred_ho / norm
        self.pred_test = pred_test / norm


class DiffAlg(Forecast):

    def __init__(self, models, variable, corr_coeff, order, y_ho, y_test):
        name = "DiffAlg_" + variable + "_" + corr_coeff + "_" + order + str(len(models))
        Forecast.__init__(self, name, models[0].y_len)
        self.corr_coeff = corr_coeff
        self.weights = {}
        self.predict(models, variable, corr_coeff, order, y_ho, y_test)
        self.plot_correlation(models)

    def predict(self, models, variable, corr_coeff, order, y_ho, y_test):

        rho_min = None

        dot = Graph(name=self.name)
        dot.attr(rankdir="RL")

        for model in models:
            model.weight = 1  # We reset the weights
            dot.node(model.name, model.name + "\n" + str(round(model.mae_ho, 2)))

        models_copy = [model for model in models]

        while len(models_copy) > 1:
            data_ho = DiffAlg.make_data_ho(models_copy, variable)

            corr_mat = DiffAlg.corr_mat(data_ho, corr_coeff)

            if rho_min is None:
                rho_min = corr_mat.min()

            if len(models_copy) > 2:
                i, j = DiffAlg.indexes(order, corr_mat)
                rho = corr_mat[i, j]
            else:
                i, j = 0, 1
                rho = corr_mat

            new_models = [model for model in models_copy if model not in (models_copy[i], models_copy[j])]

            if models_copy[i].mae_ho > models_copy[j].mae_ho:
                new_models.append(DiffAlgWgtAvg(models_copy[j], models_copy[i], rho, rho_min))
            else:
                new_models.append(DiffAlgWgtAvg(models_copy[i], models_copy[j], rho, rho_min))

            new_models[-1].computeKPIs(y_ho, y_test)

            color = DiffAlg.node_color(new_models, models_copy[i].mae_ho, models_copy[j].mae_ho)
            dot.node(new_models[-1].name, label=str(round(new_models[-1].mae_ho, 2))+"\n"+str(round(rho, 4)), color=color)
            dot.edge(new_models[-1].name, models_copy[i].name, label=str(round(models_copy[i].weight, 2)))
            dot.edge(new_models[-1].name, models_copy[j].name, label=str(round(models_copy[j].weight, 2)))

            models_copy = new_models

        dot.render("Plots/TreePlot " + self.name, format="png", cleanup=True)

        self.pred_ho = models_copy[0].pred_ho
        self.pred_test = models_copy[0].pred_test

        self.record_weights(models)

    @staticmethod
    def corr_mat(data_ho, corr_coeff):
        if corr_coeff == "Pearson":
            corr_mat = np.corrcoef(np.reshape(np.vstack(data_ho), (len(data_ho), -1)))
            if len(data_ho) == 2:
                corr_mat = corr_mat[0, 1]
        elif corr_coeff == "Spearman":
            from scipy.stats import spearmanr
            corr_mat, p = spearmanr(np.reshape(np.vstack(data_ho), (len(data_ho), -1)), axis=1)
        else:
            raise AttributeError("Correlation coefficient is invalid")

        return np.nan_to_num(corr_mat)

    @staticmethod
    def plot_correlation(models):
        sns.set()
        names = [model.name for model in models]
        preds_ho = [model.pred_ho for model in models]
        error_ho = [model.error_ho for model in models]
        pred_mat = np.corrcoef(np.reshape(np.vstack(preds_ho), (len(preds_ho), -1)))
        error_mat = np.corrcoef(np.reshape(np.vstack(error_ho), (len(error_ho), -1)))

        f, ax = plt.subplots(2, 1, figsize=(9, 12), sharex=True)
        ax[0].set_title("Predictions correlation")
        ax[1].set_title("Errors correlation")
        sns.heatmap(pred_mat, annot=True, annot_kws={"size": 10}, xticklabels=names, yticklabels=names,
                    fmt='.3', linewidths=.5, ax=ax[0])
        sns.heatmap(error_mat, annot=True, annot_kws={"size": 10}, xticklabels=names, yticklabels=names,
                    fmt='.3', linewidths=.5, ax=ax[1])

        plt.savefig('Plots/HeatmapPredsErrors.png')


    @staticmethod
    def indexes(order, corr_mat):
        if order == "UP":
            return np.unravel_index(corr_mat.argmin(), corr_mat.shape)
        elif order == "DN":
            np.fill_diagonal(corr_mat, 0)
            return np.unravel_index(corr_mat.argmax(), corr_mat.shape)
        else:
            raise AttributeError("Order is invalid")

    @staticmethod
    def node_color(new_models, i_mae, j_mae):
        if new_models[-1].mae_ho < i_mae and new_models[-1].mae_ho < j_mae:
            return "green"
        elif new_models[-1].mae_ho > i_mae or new_models[-1].mae_ho > j_mae:
            return "red"
        else:
            return "black"

    @staticmethod
    def make_data_ho(models_copy, variable):
        if variable == "pred":
            data_ho = [model.pred_ho for model in models_copy]
        elif variable == "error":
            data_ho = [model.error_ho for model in models_copy]
        else:
            raise AttributeError("The attribute variable is not valid")
        return data_ho


    def record_weights(self, models):
        for model in models:
            self.weights[model.name] = model.weight

        self.plot_weights()

    def plot_weights(self):
        plt.style.use('classic')
        x = list()
        y = list()

        plt.figure()

        for key in self.weights.keys():
            x.append(key)
            y.append(self.weights[key])

        sns.barplot(x=x, y=y, palette="deep")
        plt.xticks(rotation=80)

        for i in range(len(x)):
            plt.text(i, y[i]+1/(len(x)), str(round(y[i]*100, 2)) + "%", rotation=90)

        plt.tight_layout()
        plt.savefig("Plots/Weights " + self.name + ".png")


class DiffAlg2(Forecast):
    # In this ensembling, if the weighted average of two models has a higher error,
    # it discards it and the worst performing model and puts the best base model back
    # in the pool.
    def __init__(self, models, variable, corr_coeff, order, y_ho, y_test):
        name = "DiffAlg2_" + variable + "_" + corr_coeff + "_" + order + str(len(models))
        Forecast.__init__(self, name, models[0].y_len)
        self.corr_coeff = corr_coeff
        self.weights = {}
        self.predict(models, variable, corr_coeff, order, y_ho, y_test)
        self.plot_correlation(models)

    def predict(self, models, variable, corr_coeff, order, y_ho, y_test):

        rho_min = None

        dot = Graph(name=self.name)
        dot.attr(rankdir="RL")

        for model in models:
            model.weight = 1  # We reset the weights
            dot.node(model.name, model.name + "\n" + str(round(model.mae_ho, 2)))

        models_copy = [model for model in models]

        while len(models_copy) > 1:
            data_ho = DiffAlg.make_data_ho(models_copy, variable)

            corr_mat = DiffAlg.corr_mat(data_ho, corr_coeff)

            if rho_min is None:
                rho_min = corr_mat.min()

            if len(models_copy) > 2:
                i, j = DiffAlg.indexes(order, corr_mat)
                rho = corr_mat[i, j]
            else:
                i, j = 0, 1
                rho = corr_mat

            new_models = [model for model in models_copy if model not in (models_copy[i], models_copy[j])]

            if models_copy[i].mae_ho > models_copy[j].mae_ho:
                new_model = DiffAlgWgtAvg(models_copy[j], models_copy[i], rho, rho_min)
                new_model.computeKPIs(y_ho, y_test)
                if new_model.mae_ho < models_copy[j].mae_ho:
                    new_models.append(new_model)
                    color = DiffAlg.node_color(new_models, models_copy[i].mae_ho, models_copy[j].mae_ho)
                    dot.node(new_models[-1].name,
                             label=str(round(new_models[-1].mae_ho, 2)) + "\n" + str(round(rho, 4)),
                             color=color)
                    dot.edge(new_models[-1].name, models_copy[i].name, label=str(round(models_copy[i].weight, 2)))
                    dot.edge(new_models[-1].name, models_copy[j].name, label=str(round(models_copy[j].weight, 2)))
                else:
                    models_copy[i].weight = 0
                    models_copy[j].weight = 1
                    new_models.append(models_copy[j])
            else:
                new_model = DiffAlgWgtAvg(models_copy[i], models_copy[j], rho, rho_min)
                new_model.computeKPIs(y_ho, y_test)
                if new_model.mae_ho < models_copy[i].mae_ho:
                    new_models.append(new_model)
                    color = DiffAlg.node_color(new_models, models_copy[i].mae_ho, models_copy[j].mae_ho)
                    dot.node(new_models[-1].name,
                             label=str(round(new_models[-1].mae_ho, 2)) + "\n" + str(round(rho, 4)),
                             color=color)
                    dot.edge(new_models[-1].name, models_copy[i].name, label=str(round(models_copy[i].weight, 2)))
                    dot.edge(new_models[-1].name, models_copy[j].name, label=str(round(models_copy[j].weight, 2)))
                else:
                    models_copy[i].weight = 1
                    models_copy[j].weight = 0
                    new_models.append(models_copy[i])

            models_copy = new_models

        dot.render("Plots/TreePlot " + self.name, format="png", cleanup=True)

        self.pred_ho = models_copy[0].pred_ho
        self.pred_test = models_copy[0].pred_test

        self.record_weights(models)

    @staticmethod
    def corr_mat(data_ho, corr_coeff):
        if corr_coeff == "Pearson":
            corr_mat = np.corrcoef(np.reshape(np.vstack(data_ho), (len(data_ho), -1)))
            if len(data_ho) == 2:
                corr_mat = corr_mat[0, 1]
        elif corr_coeff == "Spearman":
            from scipy.stats import spearmanr
            corr_mat, p = spearmanr(np.reshape(np.vstack(data_ho), (len(data_ho), -1)), axis=1)
        else:
            raise AttributeError("Correlation coefficient is invalid")

        return np.nan_to_num(corr_mat)

    @staticmethod
    def plot_correlation(models):
        sns.set()
        names = [model.name for model in models]
        preds_ho = [model.pred_ho for model in models]
        error_ho = [model.error_ho for model in models]
        pred_mat = np.corrcoef(np.reshape(np.vstack(preds_ho), (len(preds_ho), -1)))
        error_mat = np.corrcoef(np.reshape(np.vstack(error_ho), (len(error_ho), -1)))

        f, ax = plt.subplots(2, 1, figsize=(9, 12), sharex=True)
        ax[0].set_title("Predictions correlation")
        ax[1].set_title("Errors correlation")
        sns.heatmap(pred_mat, annot=True, annot_kws={"size": 10}, xticklabels=names, yticklabels=names,
                    fmt='.3', linewidths=.5, ax=ax[0])
        sns.heatmap(error_mat, annot=True, annot_kws={"size": 10}, xticklabels=names, yticklabels=names,
                    fmt='.3', linewidths=.5, ax=ax[1])


    @staticmethod
    def indexes(order, corr_mat):
        if order == "UP":
            return np.unravel_index(corr_mat.argmin(), corr_mat.shape)
        elif order == "DN":
            np.fill_diagonal(corr_mat, 0)
            return np.unravel_index(corr_mat.argmax(), corr_mat.shape)
        else:
            raise AttributeError("Order is invalid")

    @staticmethod
    def node_color(new_models, i_mae, j_mae):
        if new_models[-1].mae_ho < i_mae and new_models[-1].mae_ho < j_mae:
            return "green"
        elif new_models[-1].mae_ho > i_mae or new_models[-1].mae_ho > j_mae:
            return "red"
        else:
            return "black"

    @staticmethod
    def make_data_ho(models_copy, variable):
        if variable == "pred":
            data_ho = [model.pred_ho for model in models_copy]
        elif variable == "error":
            data_ho = [model.error_ho for model in models_copy]
        else:
            raise AttributeError("The attribute variable is not valid")
        return data_ho


    def record_weights(self, models):
        for model in models:
            self.weights[model.name] = model.weight

        self.plot_weights()

    def plot_weights(self):
        plt.style.use('classic')
        x = list()
        y = list()

        plt.figure()

        for key in self.weights.keys():
            x.append(key)
            y.append(self.weights[key])

        sns.barplot(x=x, y=y, palette="deep")
        plt.xticks(rotation=80)

        for i in range(len(x)):
            plt.text(i, y[i]+1/(len(x)), str(round(y[i]*100, 2)) + "%", rotation=90)

        plt.tight_layout()
        plt.savefig("Plots/Weights " + self.name + ".png")


class DiffAlgWgtAvg(Forecast):

    def __init__(self, model1, model2, rho, rho_min):
        name = model1.name + model2.name
        Forecast.__init__(self, name, model1.y_len)
        self.model1 = model1
        self.model2 = model2
        self.predict(model1, model2, rho, rho_min)

    def predict(self, model1, model2, rho, rho_min):

        d_mae = model2.mae_ho / model1.mae_ho
        w = min(1, self.w_corr(rho, rho_min, d_mae))
        model1.update_weight(w)
        model2.update_weight(1 - w)
        self.pred_ho = w * model1.pred_ho + (1 - w) * model2.pred_ho
        self.pred_test = w * model1.pred_test + (1 - w) * model2.pred_test

    @staticmethod
    def w_corr(rho, rho_min, d_mae):
        return (((rho - 0) / (1 - 0)) ** ((1.25 - d_mae) / (d_mae - 1)) + 1) / 2

    @staticmethod
    def plt_w(rho_min):
        import matplotlib.pyplot as plt

        rho = np.arange(rho_min, 1.005, 0.005)

        d_maes = [1.0001, 1.05, 1.1, 1.15, 1.2, 1.2499]

        plt.figure()

        for d_mae in d_maes:
            plt.plot(rho, DiffAlgWgtAvg.w_corr(rho, rho_min, d_mae), label="dMAE=" + str(round(d_mae, 3)))

        plt.xlabel('Correlation')
        plt.ylabel('Weight for best performing')
        plt.title('Weight given correlation, various dMAEs')
        plt.legend()
        plt.savefig("Plots/WeightingFunction")

    def update_weight(self, weight):
        self.model1.update_weight(weight)
        self.model2.update_weight(weight)
        self.weight *= weight


class MLCombine(Forecast):

    def __init__(self, models, comb_model, y_ho):
        name = comb_model + "." + str(len(models))
        Forecast.__init__(self, name, models[0].y_len)
        self.predict(models, comb_model, y_ho)

    def predict(self, models, comb_model, y_ho):
        print("Combining models with "+comb_model)

        preds = list()
        for model in models:
            preds.append(model.pred_ho)

        preds = np.hstack(preds)

        if comb_model == "ETR":
            meta_model = ETR(24, self.y_len)
        elif comb_model == "Tree":
            meta_model = Tree(24, self.y_len)
        elif comb_model == "Forest":
            meta_model = Forest(24, self.y_len)
        elif comb_model == "NN":
            meta_model = NN(24, self.y_len)
        elif comb_model == "Ada":
            meta_model = AdaBoost(24, self.y_len)
        elif comb_model == "GBoost":
            meta_model = GBoost(24, self.y_len)
        elif comb_model == "XGBoost":
            meta_model = XGBoost(24, self.y_len)
        else:
            raise AttributeError("Combination model invalid")

        meta_model.train(preds, y_ho)

        preds_ho = list()
        preds_test = list()
        preds_train = list()
        for model in models:
            preds_ho.append(model.pred_ho)
            preds_test.append(model.pred_test)

        preds_ho = np.hstack(preds_ho)
        preds_test = np.hstack(preds_test)

        meta_model.predict(preds_train, preds_ho, preds_test)

        self.pred_ho = meta_model.pred_ho
        self.pred_test = meta_model.pred_test
