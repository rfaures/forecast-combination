"""from ete3 import Tree, TreeStyle


t = Tree()
t.add_child("First")
ts = TreeStyle()
ts.mode = "c"
ts.arc_span = 180
ts.arc_start = 180
ts.show_leaf_name = True
ts.show_branch_length = True
t.show(tree_style=ts)"""

import numpy as np
import matplotlib.pyplot as plt
from copy import deepcopy
from KPIs import MAE


def w_corr(Cmin, C, dMAE):
    Cmin *= 0.9
    return min(1,(((C-Cmin)/(1.0-Cmin))**((1.25-dMAE)/(dMAE-1))+1)/2)


def tree_diff_alg(preds_or, y, preds_or2, maes_or):
    # Need to deep copy 

    names = ["ETS", "Tree12", "Forest12", "ETR12", "Ada12", "GB12", "XGB12", "NN12", "Tree24", "Forest24", "ETR24", "Ada24", "GB24", "XGB24", "NN24"]
    preds = deepcopy(preds_or)
    preds2 = deepcopy(preds_or2)
    maes = deepcopy(maes_or)

    corr_mat = np.corrcoef(np.reshape(np.vstack(preds), (len(preds), -1)))

    corr_mat = np.nan_to_num(corr_mat)
    Cmin = corr_mat.min()

    while len(preds) > 1:

        print(str(len(preds))+" models left.")

        preds_st = np.vstack(preds)
        preds_st = np.reshape(preds_st, (len(preds), -1))

        corr_mat = np.corrcoef(preds_st)

        corr_mat = np.nan_to_num(corr_mat)

        if len(preds) > 2:
            i, j = np.unravel_index(corr_mat.argmin(), corr_mat.shape)
            C = corr_mat[i, j]
        else:
            i, j = 0, 1
            C = corr_mat[0, 1]

        # We combine most correlated models
        if maes[i] > maes[j]: # k is the index of the worst model and l is the index of the best
            k, l = i, j
        else:
            k, l = j, i

        print("We combine " + names[l] + " (MAE: "+ str(round(maes[l], 2)) + ") and " + names[k] + " (MAE: " + str(round(maes[k], 2))+")")

        dMAE = maes[k] / maes[l]
        w = w_corr(Cmin, C, dMAE)

        print("Weight of " + str(w))
        preds[l] = w * preds[l] + (1-w) * preds[k]
        preds2[l] = w * preds2[l] + (1-w) * preds2[k]
        maes[l] = MAE(y, preds[l])

        print("It gives us a MAE of " + str(round(maes[l], 2))+"\n")

        del preds[k]
        del preds2[k]
        del names[k]
        del maes[k]

    predss = [preds, preds2]

    return predss
