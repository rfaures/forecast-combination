import seaborn as sns
from Kaggle import *

sns.set()


names = ['ETS', 'Tree12', 'Forest12', 'ETR12', 'AdaBoost12', 'GBoost12', 'XGB12', 'NN12', 'Tree24', 'Forest24', 'ETR24', 'AdaBoost24', 'GBoost24', 'XGB24', 'NN24']


preds_st = pd.read_excel("preds_ho.xlsx", index_col=0)
preds_st = np.array(preds_st)
preds_st = np.reshape(preds_st, (15, -1))

pears_mat = np.corrcoef(preds_st)
spear_mat, p = spearmanr(preds_st, axis=1)


f, ax = plt.subplots(2, 1, figsize=(9, 12), sharex=True)
ax[0].set_title("Pearson")
ax[1].set_title("Spearman")
sns.heatmap(pears_mat, annot=True, annot_kws={"size": 10}, xticklabels=names, vmin=0.8, yticklabels=names, fmt='.3', linewidths=.5, ax=ax[0])
sns.heatmap(spear_mat, annot=True, annot_kws={"size": 10}, xticklabels=names, vmin=0.8, yticklabels=names, fmt='.3', linewidths=.5, ax=ax[1])
plt.savefig('Plots/HeatmapPearsSpear.png')


