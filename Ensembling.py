import numpy as np
from copy import deepcopy
from scipy.stats import spearmanr
from KPIs import MAE

def select(preds_or, test_or, train_or, maes_or, names_or, min_models=2, method="Spearman"):
    # Selects models based on correlation and accuracy

    # On doit les deep copy pour pas altérer notre liste de départ
    preds = deepcopy(preds_or)
    maes = deepcopy(maes_or)
    test = deepcopy(test_or)
    train = deepcopy(train_or)
    names = deepcopy(names_or)

    while len(preds) > min_models:
        preds_st = np.vstack(preds)
        preds_st = np.reshape(preds_st, (len(preds), -1))
        if method == "Pearson":
            corr_mat = np.corrcoef(preds_st)
        elif method == "Spearman":
            corr_mat, p = spearmanr(preds_st, axis=1)
        corr_mat = np.nan_to_num(corr_mat)  # On remplace les NaN par zero A CHANGER
        np.fill_diagonal(corr_mat, 0)  # On annule la diagonale
        i, j = np.unravel_index(corr_mat.argmax(), corr_mat.shape)

        # On supprime le moins bon modèle
        if maes[i] > maes[j]:
            k = i
        else:
            k = j
        del preds[k]
        del test[k]
        del maes[k]
        del train[k]
        del names[k]

    print(names)

    return preds, test, train
