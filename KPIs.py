import numpy as np


def MAE(y, pred):
    # Returns MAE in %

    return (np.mean(abs(y - pred)) / np.mean(y)) * 100


def RMSE(y, pred):
    # Returns RMSE in %

    return (((np.mean((y - pred) ** 2)) ** 0.5) / np.mean(y)) * 100


def bias(y, pred):
    # Returns bias

    return np.mean(pred - y)

