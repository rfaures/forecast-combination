# Forecast Combination

Master's thesis on the subject of forecast pooling/combination.
**Have a look at the final PDF [here](https://www.academia.edu/41363368/Forecast_Pooling_an_approach_based_on_correlation) !**

Files in the repo:
- Python
  -
    - Main.py
        - Code that runs several iterations of model training and ensembling
        and conceals the data into Excel files
    - Kaggle.py
        - Main file, runs iterations stocked in the .xlsx files
    - KPIs.py
        - All the functions that compute the different KPIs
    - Ensembling.py
        - The ensembling functions used to combine base forecasts
    - Plot_Correlation.py
        - Plots the heatmap of the correlations between the models
        based on a single iteration
- Excel
  -
    - clean_data.xlsx
        - Contains the data processed with Pandas
    - Holdouts.xlsx
        - Contains the KPIs for each iteration and each base model
        and ensembling method, as well as on average, on the Holdout data
    - predsETS_ho.xlsx & predsETS_test.xlsx
        - Results of ETS forecasting, for both HO and Test
        
- Plots
  - 
This directory contains all the plots created in this project.

- Results
  -
This directory contains the KPIs for each iteration and each base model
and ensembling method, as well as on average, on the Holdout and Test data