from Plot_tree import *
from POO import *


def prepare_data():
    data = pd.read_csv("car_sales.csv")

    data['Period'] = data['Year'].astype(str) + '-' + data['Month'].astype(str)

    data['Period'] = pd.to_datetime(data['Period']).dt.strftime("%Y-%m")

    df = data.pivot_table(index="Make", columns="Period", values="Quantity",
                          aggfunc='sum', fill_value=0)

    df.to_excel("clean_data.xlsx")


def datasetsML(D, x_len=12, y_len=3, y_test_len=12, holdout=12):
    # Formating the data

    periods = D.shape[1]

    # Training set creation: run through all the possible time windows
    loops = periods + 1 - x_len - y_len - y_test_len
    train = []
    for col in range(loops):
        train.append(D[:, col:col + x_len + y_len])
    train = np.vstack(train)
    X_train, Y_train = np.split(train, [x_len], axis=1)

    rows = D.shape[0]
    if holdout > 0:
        X_train, X_holdout = np.split(X_train, [-rows * holdout], axis=0)
        Y_train, Y_holdout = np.split(Y_train, [-rows * holdout], axis=0)
    else:
        X_holdout = np.array([])
        Y_holdout = np.array([])

    # Test set creation: unseen "future" data with the demand just before
    max_col_test = periods - x_len - y_len + 1
    test = []
    for col in range(loops, max_col_test):
        test.append(D[:, col:col + x_len + y_len])
    test = np.vstack(test)
    X_test, Y_test = np.split(test, [x_len], axis=1)

    # Data formatting is needed if we only want to predict a single period
    if y_len == 1:
        Y_train = Y_train.ravel()
        Y_test = Y_test.ravel()
        Y_holdout = Y_holdout.ravel()

    return X_train, Y_train, X_holdout, Y_holdout, X_test, Y_test


def create_models(d, y_len):

    base_models = list()

    print("Training\n______________")
    for x_len in [12, 24]:
        x_train, y_train, x_ho, y_ho, x_test, y_test = datasetsML(d, x_len=x_len, y_len=y_len)
        base_models.append(Forest(x_len, y_len))
        base_models.append(ETR(x_len, y_len))
        base_models.append(AdaBoost(x_len, y_len))
        base_models.append(GBoost(x_len, y_len))

        base_models.append(NN(x_len, y_len))

        for model in base_models:
            if model.x_len == x_len:
                model.train(x_train, y_train)
                model.predict(x_train, x_ho, x_test)
                model.computeKPIs(y_ho, y_test)

    # And the deterministic forecasts
    base_models.append(Tree(12, 3))
    base_models.append(Tree(24, 3))
    base_models.append(XGBoost(12, 3))
    base_models.append(XGBoost(24, 3))
    base_models.append(ETS())
    for base_model in base_models[-5:]:
        base_model.predict()
        base_model.computeKPIs(y_ho, y_test)

    meta_models = list()
    meta_models.append(PlainAvg(base_models))
    meta_models.append(WgtAvg(base_models))
    meta_models.append(DiffAlg(base_models, "error", "Pearson", "UP", y_ho, y_test))
    meta_models.append(DiffAlg(base_models, "error", "Pearson", "DN", y_ho, y_test))
    meta_models.append(DiffAlg2(base_models, "error", "Pearson", "UP", y_ho, y_test))
    meta_models.append(DiffAlg2(base_models, "error", "Pearson", "DN", y_ho, y_test))
    meta_models.append(MLCombine(base_models, "ETR", y_ho))
    """meta_models.append(MLCombine(base_models, "Ada", y_ho))
    meta_models.append(MLCombine(base_models, "GBoost", y_ho))
    meta_models.append(MLCombine(base_models, "XGBoost", y_ho))
    meta_models.append(MLCombine(base_models, "Tree", y_ho))
    meta_models.append(MLCombine(base_models, "Forest", y_ho))
    meta_models.append(MLCombine(base_models, "NN", y_ho))"""

    # Selection
    # The two best MAEs
    maes = []
    for model in base_models:
        maes.append(model.mae_ho)

    maes = np.array(maes)
    i = maes.argmin()
    maes[i] = None
    j = maes.argmin()

    best_models = [base_models[i], base_models[j]]

    meta_models.append(PlainAvg(best_models))
    meta_models.append(WgtAvg(best_models))

    for model in meta_models:
        model.computeKPIs(y_ho, y_test)

    return base_models + meta_models


def compare(d, y_len=3):
    # Returns two dics (HO & Test) with the values of RMSE Bias and MAE for each model

    ho = {}
    test = {}

    models = create_models(d, y_len)

    for model in models:
        ho[model.name] = model.mae_ho, model.rmse_ho, model.bias_ho
        test[model.name] = model.mae_test, model.rmse_test, model.bias_test

    return ho, test


def conceal(d, iteration):
    res = [0, 0]
    for i in range(iteration):  # For each iteration
        dics = compare(d)  # Retrieve error dics for HO and Tests
        n = 0
        for dic in dics:  # For HO then for Test
            if i == 0:  # If we are at first iteration
                models = dic.keys()
                errors = ["MAE", "RMSE", "Bias"]
                it = [list(range(iteration))+["Avg"], errors]
                res[n] = pd.DataFrame(index=models,
                                      columns=pd.MultiIndex.from_product(it, names=["Iter #", "Error"]))
            col = np.vstack(dic.values())  # Add iteration's result in corresponding column
            res[n][i] = col
            n += 1

        print("Run " + str(i + 1) + " done")

    for r in res:  # Computation of each error's average value
        r["Avg", "MAE"] = round(np.mean(r.loc[:, (slice(None), ["MAE"])], axis=1), 4)
        r["Avg", "RMSE"] = round(np.mean(r.loc[:, (slice(None), ["RMSE"])], axis=1), 4)
        r["Avg", "Bias"] = round(np.mean(r.loc[:, (slice(None), ["Bias"])], axis=1), 4)
        r.sort_values(by=("Avg", "MAE"), inplace=True)
    res[0].to_excel("Results/HO.xlsx")
    res[1].to_excel("Results/Tests.xlsx")

    return

